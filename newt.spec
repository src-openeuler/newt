Name:		newt
Version: 	0.52.24
Release: 	2
Summary: 	A library for text mode user interfaces
License: 	LGPL-2.0-only
URL: 		https://pagure.io/newt
Source: 	https://pagure.io/releases/newt/newt-%{version}.tar.gz

BuildRequires: 	gcc popt-devel python3-devel slang-devel
BuildRequires: 	docbook-utils lynx make

Provides: 	snack = %{version}-%{release}
Obsoletes: 	%{name}-static

%description
Newt is a programming library for color text-mode, widget-based user
interfaces.  Newt can be used to add stacked windows, entry widgets,
checkboxes, radio buttons, labels, plain text fields, scrollbars, etc.,
to text mode user interfaces.

This package also contains a Dialog replacement called whiptail. Newt
is based on the slang library.

%package 	devel
Summary: 	Development files for %{name}
Requires: 	slang-devel %{name}%{?_isa} = %{version}-%{release}

%description    devel
The devel for %{name}. Install %{name}-devel can help you develop
applications.

%package     -n python3-newt
%{?python_provide:%python_provide python3-newt}
Provides:       %{name}-python3 = %{version}-%{release}
Provides: 	%{name}-python3%{?_isa} = %{version}-%{release}
Obsoletes: 	%{name}-python3 < %{version}-%{release}
Summary: 	Python 3 bindings for %{name}
Requires: 	%{name}%{?_isa} = %{version}-%{release}

%description -n python3-newt
The python3-%{name} package contains the Python 3 bindings for the %{name} library
providing a python API for creating text mode interfaces.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --without-tcl
%make_build all
chmod 0644 peanuts.py popcorn.py
docbook2txt tutorial.sgml

%install
%make_install
%delete_la_and_a

%find_lang %{name}

%check
%make_build test

%files -f %{name}.lang
%doc AUTHORS README
%license COPYING
%{_bindir}/*
%{_libdir}/libnewt.so.*

%files devel
%{_includedir}/newt*
%{_libdir}/libnewt.so
%{_libdir}/pkgconfig/*

%files -n python3-newt
%doc peanuts.py popcorn.py
%{python3_sitearch}/*.*
%{python3_sitearch}/__pycache__/*.py*

%files help
%doc tutorial.* CHANGES
%{_mandir}/man1/whiptail.1*

%changelog
* Thu Dec 05 2024 Funda Wang <fundawang@yeah.net> - 0.52.24-2
- fix locale files installation
- cleanup spec

* Wed Aug  7 2024 dillon chen <dillon.chen@gmail.com> - 0.52.24-1
- Update newt version to 0.52.24 again

* Thu Aug 01 2024 yanglu <yanglu72@h-partners.com> - 0.52.23-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix license

* Thu Feb 01 2024 yanglu <yanglu72@h-partners.com> - 0.52.23-1
- Type:requirement
- Id:NA
- SUG:NA
- DESC:Revert update newt version to 0.52.24

* Wed Dec 06 2023 Vicoloa <lvkun@uniontech.com> - 0.52.24-1
- Update newt version to 0.52.24

* Tue Jan 31 2023 yanglu <yanglu72@h-partners.com> - 0.52.23-1
- Type:requirement
- Id:NA
- SUG:NA
- DESC:update newt version to 0.52.23

* Thu Oct 20 2022 yanglu <yanglu72@h-partners.com> - 0.52.21-7
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix warnings about never-read values

* Thu Apr 28 2022 yanglu <yanglu72@h-partners.com> - 0.52.21-6
- Type:requirement
- Id:NA
- SUG:NA
- DESC:rebuild package

* Thu Oct 29 2020 gaihuiying <gaihuiying1@huawei.com> - 0.52.21-4
- Type:requirement
- Id:NA
- SUG:NA
- DESC:remove python2

* Mon Oct 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.52.21-3
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:modify the location of COPYING

* Tue Sep 24 2019 wangli <wangli221@huawei.com> - 0.52.21-2
- Type:Enhance
- ID:NA
- SUG:NA
- DESC: openEuler Debranding

* Wed Sep 04 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.52.21-1
- Package init
